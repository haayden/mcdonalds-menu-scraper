const puppeteer = require('puppeteer');

const pageUrl = 'http://localhost:5000';
let browser;
let page;
jest.setTimeout(20000); // 20 second individual test timeout fail

const browserOptions = { headless: 'new' };
// options = { headless: false, slowMo: 100 };
if (process.platform === 'linux') {
  browserOptions.args = ['--no-sandbox', '--disable-dev-shm-usage'];
}

async function getXpath(expression) {
  const [el] = await page.$$(`xpath/.${expression}`);
  return el;
}

async function getTableBodyText(tableId) {
  const bodyText = await page.evaluate((_tableId) => {
    const allRowsText = [];
    document.querySelectorAll(`#${_tableId} tbody tr`).forEach((trEl) => {
      const rowText = [];
      trEl.childNodes.forEach((tdEl) => rowText.push(tdEl.textContent));
      allRowsText.push(rowText);
    });
    return allRowsText;
  }, tableId);
  return bodyText;
}

async function testTableSorting(tableId, thPositions) {
  const thSelector = (thPosition) => `#${tableId} th:nth-child(${thPosition})`;
  const initialTableData = await getTableBodyText(tableId);
  let ascTableData;

  for (const thIndex of thPositions) {
    // Click column 1st time to descending sort, and check data changed
    await page.click(thSelector(thIndex));
    const descTableData = await getTableBodyText(tableId);
    // For first loop, use default data, and after use previous loops data
    if (thPositions.indexOf(thIndex) === 0) {
      expect(initialTableData).not.toEqual(descTableData);
    } else {
      expect(ascTableData).not.toEqual(descTableData);
    }

    // Click column 2nd time to ascending sort, and check data changed
    await page.click(thSelector(thIndex));
    ascTableData = await getTableBodyText(tableId);
    expect(descTableData).not.toEqual(ascTableData);
  }
}

beforeAll(async () => {
  browser = await puppeteer.launch(browserOptions);
  page = await browser.newPage();
});

test('All tab switching and table show/hiding works', async () => {
  await page.goto(pageUrl);

  await page.waitForSelector('#nutrition-price-table', { visible: false });
  await page.type('#restaurant-choice', 'London Bridge');
  await page.keyboard.press('Enter');
  const londonBridgeTData = await getTableBodyText('nutrition-price-table');
  expect(londonBridgeTData).toEqual([
    ['Burgers', 'Cheeseburger', '0.99', '301', '304'],
    ['Fries & Sides', 'McDonald\'s Fries Large', '1.49', '444', '298'],
    ['Desserts', 'Smarties McFlurry', '0.99', '273', '276'],
    ['Chicken McNuggets® and Selects®', '20 Chicken McNuggets ShareBox', '5.19', '863', '166'],
    ['Breakfast', 'Sausage and Egg McMuffin', '2.99', '423', '141'],
    ['Burgers', 'Big Mac', '3.89', '508', '131'],
  ]); // Confirm specific restaurant data set loads

  await page.waitForSelector('#restaurant-address', { visible: true });
  await page.waitForSelector('#nutrition-price-table', { visible: true });
  await page.select('#nutrition-choice', 'kj');
  expect(
    await getXpath('//th[text()="kJ"]'),
  ).toBeTruthy(); // Confirm chosen nutrition table header exists

  await page.click('#other-menu-views-tab');
  await page.waitForSelector('#price-difference-table-button', { visible: true });

  await page.click('#price-difference-table-button');
  await page.waitForSelector('#nutrition-price-table', { visible: false });
  await page.waitForSelector('#price-difference-table', { visible: true });

  await page.click('#restaurant-table-button');
  await page.waitForSelector('#price-difference-table', { visible: false });
  await page.waitForSelector('#restaurant-table', { visible: true });

  await page.click('#all-nutrition-table-button');
  await page.waitForSelector('#restaurant-table', { visible: false });
  await page.waitForSelector('#all-nutrition-table', { visible: true });

  await page.click('#nutrition-per-gbp-view-tab');
  await page.type('#restaurant-choice', 'Victoria');
  await page.keyboard.press('Enter');
  await page.waitForSelector('#all-nutrition-table', { visible: false });
  await page.waitForSelector('#nutrition-price-table', { visible: true });
  // Expect table data to have changed from last snapshot
  const victoriaTData = await getTableBodyText('nutrition-price-table');
  expect(victoriaTData).not.toEqual(londonBridgeTData);

  await page.type('#restaurant-choice', 'Kings Cross');
  await page.keyboard.press('Enter');
  // Expect table data to have changed from last snapshot
  const kingsCrossTData = await getTableBodyText('nutrition-price-table');
  expect(kingsCrossTData).not.toEqual(victoriaTData);

  await page.type('#restaurant-choice', 'Clapham Junction');
  await page.keyboard.press('Enter');
  // Expect table data to have changed from last snapshot
  const claphamTData = await getTableBodyText('nutrition-price-table');
  expect(claphamTData).not.toEqual(kingsCrossTData);

  await page.type('#restaurant-choice', 'Edgware Road');
  await page.keyboard.press('Enter');
  // Expect table data to have changed from last snapshot
  const edgwareRoadTData = await getTableBodyText('nutrition-price-table');
  expect(edgwareRoadTData).not.toEqual(claphamTData);

  await page.type('#restaurant-choice', 'Stratford The Mall');
  await page.keyboard.press('Enter');
  // Expect table data to have changed from last snapshot
  const stratfordTData = await getTableBodyText('nutrition-price-table');
  expect(stratfordTData).not.toEqual(edgwareRoadTData);
});

test('When Nutrition Choice changes, table headers change too', async () => {
  const thXpath = (text) => `//table[@id="nutrition-price-table"]//th[text()="${text}"]`;
  await page.goto(pageUrl);

  await page.type('#restaurant-choice', 'London Bridge');
  await page.keyboard.press('Enter');
  await page.waitForSelector('#nutrition-price-table', { visible: true });
  expect(await getXpath(thXpath('Category'))).toBeTruthy();
  expect(await getXpath(thXpath('Item Name'))).toBeTruthy();
  expect(await getXpath(thXpath('Price'))).toBeTruthy();
  expect(await getXpath(thXpath('kcal'))).toBeTruthy();
  expect(await getXpath(thXpath('kcal per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'kj');
  expect(await getXpath(thXpath('kJ'))).toBeTruthy();
  expect(await getXpath(thXpath('kJ per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'fat');
  expect(await getXpath(thXpath('Fat'))).toBeTruthy();
  expect(await getXpath(thXpath('Fat per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'saturatedFat');
  expect(await getXpath(thXpath('Sat Fat'))).toBeTruthy();
  expect(await getXpath(thXpath('Sat Fat per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'carbohydrate');
  expect(await getXpath(thXpath('Carb'))).toBeTruthy();
  expect(await getXpath(thXpath('Carb per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'sugar');
  expect(await getXpath(thXpath('Sugar'))).toBeTruthy();
  expect(await getXpath(thXpath('Sugar per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'fibre');
  expect(await getXpath(thXpath('Fibre'))).toBeTruthy();
  expect(await getXpath(thXpath('Fibre per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'protein');
  expect(await getXpath(thXpath('Protein'))).toBeTruthy();
  expect(await getXpath(thXpath('Protein per £'))).toBeTruthy();

  await page.select('#nutrition-choice', 'salt');
  expect(await getXpath(thXpath('Salt'))).toBeTruthy();
  expect(await getXpath(thXpath('Salt per £'))).toBeTruthy();
});

test(
  'When Restaurant Choice is clicked, it\'s value is cleared so datalist shows again',
  async () => {
    const thXpath = (text) => `//table[@id="nutrition-price-table"]//th[text()="${text}"]`;
    await page.goto(pageUrl);

    await page.type('#restaurant-choice', 'London Bridge');
    await page.keyboard.press('Enter');
    expect(
      await page.$eval('#restaurant-choice', (e) => e.value),
    ).toEqual('London Bridge');
    await page.click('#restaurant-choice');
    expect(
      await page.$eval('#restaurant-choice', (e) => e.value),
    ).toEqual('');

    // Ensure nutrition can still be changed with an empty restaurant-choice
    await page.select('#nutrition-choice', 'protein');
    expect(await getXpath(thXpath('Protein'))).toBeTruthy();
    expect(await getXpath(thXpath('Protein per £'))).toBeTruthy();
  },
);

test(
  'When user agent Firefox, Restaurant Choice is a working Select element instead',
  async () => {
    await page.setUserAgent(
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
    );
    await page.goto(pageUrl);

    // Confirm Select element has expected options
    const optionsArray = await page.evaluate(() => {
      const stringArray = [];
      const htmlOptionsCollection = document.getElementById('restaurant-choice').options;
      for (const optionEl of htmlOptionsCollection) {
        stringArray.push(optionEl.label);
      }
      return stringArray;
    });
    expect(optionsArray).toEqual(
      [
        'Click to select',
        'Clapham Junction',
        'Edgware Road',
        'Kings Cross',
        'London Bridge',
        'Stratford The Mall',
        'Victoria',
      ],
    );

    await page.select('#restaurant-choice', 'London Bridge');
    await page.waitForSelector('#restaurant-address', { visible: true });
    expect(
      await page.$eval('#restaurant-address', (e) => e.textContent),
    ).toEqual('Tooley St, Bridge, London, SE1 2TF');
    await page.waitForSelector('#nutrition-price-table', { visible: true });
  },
);

test('When geo location set, Find Closest selects nearest restaurant', async () => {
  // Permit page accessing geolocation and spoof lat-long for London Bridge bridge
  const context = browser.defaultBrowserContext();
  await context.overridePermissions(pageUrl, ['geolocation']);
  await page.setGeolocation({ latitude: 51.5077296, longitude: -0.0889154 });
  await page.goto(pageUrl);

  // Closest restaurant should be London Bridge, about 100 metres away
  await page.click('#find-closest');
  expect(
    await page.$eval('#restaurant-choice', (e) => e.value),
  ).toEqual('London Bridge');
  await page.waitForSelector('#nutrition-price-table', { visible: true });
});

test(
  'When > 4 item price restaurants in Price Difference Table, click-to-show-all works',
  async () => {
    await page.goto(pageUrl);
    await page.click('#other-menu-views-tab');
    await page.click('#price-difference-table-button');
    await page.waitForSelector('#price-difference-table', { visible: true });

    // Maximum of 3 restaurants show in list by default, confirm that
    expect(
      await page.$eval(
        '#price-difference-table tbody tr:nth-child(3) td:nth-child(6)',
        (e) => e.textContent,
      ),
    ).toEqual('Clapham JunctionEdgware RoadLondon Bridge…Click to show 1 more');

    // After click, confirm the additional 1 is now shown
    await (await getXpath('//a[text()="Click to show 1 more"]')).click();
    expect(
      await page.$eval(
        '#price-difference-table tbody tr:nth-child(3) td:nth-child(6)',
        (e) => e.textContent,
      ),
    ).toEqual('Clapham JunctionEdgware RoadLondon BridgeVictoria');
  },
);

test('Price Difference Table has expected headers', async () => {
  const thXpath = (text) => `//table[@id="price-difference-table"]//th[text()="${text}"]`;
  await page.goto(pageUrl);
  await page.click('#other-menu-views-tab');
  await page.click('#price-difference-table-button');
  await page.waitForSelector('#price-difference-table', { visible: true });

  expect(await getXpath(thXpath('Item Name'))).toBeTruthy();
  expect(await getXpath(thXpath('Difference'))).toBeTruthy();
  expect(await getXpath(thXpath('Lowest £'))).toBeTruthy();
  expect(await getXpath(thXpath('Restaurants'))).toBeTruthy();
  expect(await getXpath(thXpath('Highest £'))).toBeTruthy();
});

test('Restaurant Table has expected headers', async () => {
  const thXpath = (text) => `//table[@id="restaurant-table"]//th[text()="${text}"]`;
  await page.goto(pageUrl);
  await page.click('#other-menu-views-tab');
  await page.click('#restaurant-table-button');
  await page.waitForSelector('#restaurant-table', { visible: true });

  expect(await getXpath(thXpath('Restaurant Name'))).toBeTruthy();
  expect(await getXpath(thXpath('JE page'))).toBeTruthy();
  expect(await getXpath(thXpath('Menu Items'))).toBeTruthy();
  expect(await getXpath(thXpath('Avg Item Price'))).toBeTruthy();
  expect(await getXpath(thXpath('Avg Rating'))).toBeTruthy();
  expect(await getXpath(thXpath('Reviews'))).toBeTruthy();
});

test('All Nutrition Table has expected headers', async () => {
  const thXpath = (text) => `//table[@id="all-nutrition-table"]//th[text()="${text}"]`;
  await page.goto(pageUrl);
  await page.click('#other-menu-views-tab');
  await page.click('#all-nutrition-table-button');
  await page.waitForSelector('#all-nutrition-table', { visible: true });

  expect(await getXpath(thXpath('Item Name'))).toBeTruthy();
  expect(await getXpath(thXpath('kCal'))).toBeTruthy();
  expect(await getXpath(thXpath('kJ'))).toBeTruthy();
  expect(await getXpath(thXpath('Fat'))).toBeTruthy();
  expect(await getXpath(thXpath('Sat Fat'))).toBeTruthy();
  expect(await getXpath(thXpath('Carb'))).toBeTruthy();
  expect(await getXpath(thXpath('Sugar'))).toBeTruthy();
  expect(await getXpath(thXpath('Fibre'))).toBeTruthy();
  expect(await getXpath(thXpath('Protein'))).toBeTruthy();
  expect(await getXpath(thXpath('Salt'))).toBeTruthy();
  expect(await getXpath(thXpath('R Availability'))).toBeTruthy();
});

test(
  'When Other Menu Views button is pressed, side effects works',
  async () => {
    await page.goto(pageUrl);

    await page.type('#restaurant-choice', 'London Bridge');
    await page.keyboard.press('Enter');
    await page.waitForSelector('#nutrition-price-table', { visible: true });

    await page.click('#other-menu-views-tab');
    await page.click('#price-difference-table-button');
    await page.waitForSelector('#price-difference-table', { visible: true });

    // Nutrition Per £ elements are reset to default state
    expect(
      await page.$eval('#restaurant-choice', (e) => e.value),
    ).toEqual('');
    expect(
      await page.$eval('#find-closest', (e) => e.style.display),
    ).not.toEqual('none');
    expect(
      await page.$eval('#restaurant-address', (e) => e.textContent),
    ).toEqual('');
    expect(
      await page.$eval('#nutrition-choice', (e) => e.disabled),
    ).toBe(true);

    // Clicked button is disabled
    expect(
      await page.$eval('#price-difference-table-button', (e) => e.disabled),
    ).toBe(true);
  },
);

test(
  'All sortable table headers change table data, and default sort is as expected',
  async () => {
    await page.goto(pageUrl);

    await page.type('#restaurant-choice', 'London Bridge');
    await page.keyboard.press('Enter');
    await page.waitForSelector('#nutrition-price-table', { visible: true });
    let initialTData = await getTableBodyText('nutrition-price-table');
    await testTableSorting('nutrition-price-table', [1, 2, 3, 4, 5]);
    await page.click('#nutrition-price-table th:nth-child(5)');
    const descNutrientPerGbpTData = await getTableBodyText('nutrition-price-table');
    // Confirm 5th column (Nutrition per £) is default sort
    expect(initialTData).toEqual(descNutrientPerGbpTData);

    await page.click('#other-menu-views-tab');
    await page.click('#price-difference-table-button');
    await page.waitForSelector('#price-difference-table', { visible: true });
    initialTData = await getTableBodyText('price-difference-table');
    await testTableSorting('price-difference-table', [1, 2, 3, 4, 5, 6]);
    await page.click('#price-difference-table th:nth-child(2)');
    const descDifferenceTData = await getTableBodyText('price-difference-table');
    // Confirm 2nd column (Difference) is default sort
    expect(initialTData).toEqual(descDifferenceTData);

    await page.click('#other-menu-views-tab');
    await page.click('#restaurant-table-button');
    await page.waitForSelector('#restaurant-table', { visible: true });
    initialTData = await getTableBodyText('restaurant-table');
    await testTableSorting('restaurant-table', [1, 3, 4]);
    await page.click('#restaurant-table th:nth-child(4)');
    const descAverageItemPriceTData = await getTableBodyText('restaurant-table');
    // Confirm 4th column (Average Item Price) is default sort
    expect(initialTData).toEqual(descAverageItemPriceTData);

    await page.click('#other-menu-views-tab');
    await page.click('#all-nutrition-table-button');
    await page.waitForSelector('#all-nutrition-table', { visible: true });
    initialTData = await getTableBodyText('all-nutrition-table');
    await testTableSorting('all-nutrition-table', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    await page.click('#all-nutrition-table th:nth-child(1)');
    const descItemNameTData = await getTableBodyText('all-nutrition-table');
    // Confirm 1st column (Item Name) is default sort
    expect(initialTData).toEqual(descItemNameTData);
  },
);

afterAll(() => browser.close());
