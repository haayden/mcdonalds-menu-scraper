const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
// eslint-disable-next-line import/no-unresolved
const clientTemplateData = require('./tmp/clientTemplateData.json');

module.exports = {
  entry: {
    app: './src/client/app.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/client/index.html.ejs',
      templateParameters: {
        ...clientTemplateData,
        buildTimestampIso8601String: new Date().toISOString(),
      },
      inject: 'body',
      minify: true,
    }),
    new MiniCssExtractPlugin({ filename: '[name].[contenthash].css' }),
  ],
  module: {
    rules: [
      { test: /\.css$/i, use: [MiniCssExtractPlugin.loader, 'css-loader'] },
    ],
  },
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'public'),
    clean: true,
  },
  optimization: {
    minimize: true,
    // Minimize CSS along with default TerserPlugin for JS
    minimizer: [new CssMinimizerPlugin(), '...'],
    splitChunks: {
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
};
