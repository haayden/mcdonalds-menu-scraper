FROM node:22-alpine3.19

# Bits taken from: https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#running-on-alpine
# Install latest Chromium, version viewable here: https://pkgs.alpinelinux.org/package/edge/community/x86_64/chromium
RUN apk add --no-cache chromium
# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

RUN mkdir -p /app

WORKDIR /app
