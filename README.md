# 🍟 McDonald's Menu Scraper

This project web scrapes McDonald's menu nutritional and pricing data, and then generates a website which presents the scraped data in fun tabular ways.

Using Gitlab Pages, the data/website is automatically updated daily, and is viewable here: <https://haayden.gitlab.io/mcdonalds-menu-scraper/>.

## Requirements

Either [Docker](https://docs.docker.com/get-docker/) or [Google Chrome](https://www.google.co.uk/chrome/)/[Chromium](https://www.chromium.org/getting-involved/download-chromium) and [Node.js](https://nodejs.org/en/)

## Setup

Install project dependencies using: `npm install`

## Usage

Main `npm run` commands:

- `server:scrape`
  - Uses Puppeteer to web scrape Just Eat and McDonald's Nutritional Calculator data, and places this in files **tmp\jeRestaurantsData.json** and **tmp\nutritionData.json**
- `server:build-client-data`
  - Using scraped data, builds additional data files which the website will use during build time. Additional data files are **tmp\clientJsData.json** and **tmp\clientTemplateData.json**
- `client:start`
  - Useful for local development, this builds and serves the website at <http://localhost:8080>. Automatically updates the browser on changes
- `client:build`
  - Using client data, builds production optimised website static assets to folder `public`
- `client:serve`
  - Starts a local HTTP server to serve `public` at <http://localhost:5000>
- `lint`
  - Runs all linters to check project files for any rule violations. Configured linters:
    - **eslint** to check **.js** files against rules set by Airbnb **JavasScript Style Guide** and **Jest Recommended**
    - **stylelint** to check **.css** files against rules set by **Stylelint Config Standard** and alphabetised property ordering
    - **markdownlint** to check **.md** against it's default rules
- `test`
  - Runs all Jest tests. To run a single test, in test file contents amend **test** to **test.only**
- `test:unit`
  - Runs Jest units tests, denoted by filename pattern ***.test.js**.
- `test:e2e`
  - Runs Jest and Puppeteer integration tests, denoted by filename pattern **\*.e2e.test.js**.

## Docker

If you're having issues running the project in your local environment, try running it within containers using `docker compose`.

### Docker Setup

Build container, and install project JS dependencies using:

```bash
docker compose run --rm app npm install
```

### Docker Usage

See previous Usage sections for available `npm run` commands.

To run an `npm run` command inside a container:

```bash
docker compose run --rm --service-ports app npm run server:scrape
```
