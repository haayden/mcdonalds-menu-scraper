const { setTimeout } = require('node:timers/promises');

const { hasOwnProperty } = Object.prototype;
const puppeteer = require('puppeteer');
const jeRestaurantSlugs = require('./jeRestaurantSlugs.json');
const {
  writeFileSync,
  normaliseName,
  addItemPriceToRestaurantData,
  stringIsJson,
} = require('./utilities');

let browser;

async function initialiseBrowser() {
  const options = { headless: 'new' };
  // const options = { headless: false, slowMo: 100 };

  if (process.platform === 'linux') {
    options.args = ['--no-sandbox', '--disable-dev-shm-usage'];
  }

  browser = await puppeteer.launch(options);
}

// Alter user agent so we look like a normal 'headful' Chrome browser, not headless
async function setUserAgentHeadfulChrome(page) {
  const headlessUserAgent = await browser.userAgent();
  await page.setUserAgent(headlessUserAgent.replace('Headless', ''));
}

// Abort redundant requests to reduce scraping burden and speed up task run
async function abortRedundantRequests(page, extraUrlPart = null) {
  const urlParts = [
    '.svg',
    '.woff2',
    'adobedtm.com',
    'browser-intake-datadoghq.eu',
    'cloudinary.com',
    'demdex.net',
    'go-mpulse.net',
    'googletagmanager.com',
    'hotjar.com',
    'jet-external.com',
    'maps.googleapis.com',
    'milestoneinternet.com',
    'onetrust.com',
    'paypal.com',
    'run-hlx.page',
    'scene7.com',
    'sentry.io',
    'tealiumiq.com',
    'usabilla.com',
  ];
  if (extraUrlPart) urlParts.push(extraUrlPart);

  await page.setRequestInterception(true);

  page.on('request', (request) => {
    if (request.resourceType() === 'image') request.abort();
    else if (urlParts.some((part) => request.url().includes(part))) {
      request.abort();
    } else request.continue();
  });
}

// Try 3 times to page.goto and raise error if persistently errors
async function resilientGoto(page, url) {
  for await (const retryCount of [1, 2, 3]) {
    try {
      await page.goto(url);
      break;
    } catch (error) {
      console.error(`page.goto() ${error}`);
      if (retryCount === 3) {
        console.error('Throwing 3rd error.');
        throw (error);
      }
    }
  }
}

// Try 3 times to page.waitForResponse and raise error if persistently errors
async function resilientWaitForResponse(page, callback) {
  let httpResponse = null;

  for await (const retryCount of [1, 2, 3]) {
    try {
      httpResponse = await page.waitForResponse(callback);
      break;
    } catch (error) {
      console.error(`page.waitForResponse() ${error}`);
      if (retryCount === 3) {
        console.error('Throwing 3rd error.');
        throw (error);
      }
    }
  }
  return httpResponse;
}

async function scrapeJustEatRestaurants() {
  console.log('scrapeJustEatRestaurants() starts.');

  const page = await browser.newPage();
  await setUserAgentHeadfulChrome(page);
  abortRedundantRequests(page, '.css');

  // Collect all restaurants data to be later written to JSON file
  const jeRestaurantsData = {};

  // Sourced from: https://www.google.co.uk/maps?hl=en&q=mcdonalds+london
  //               https://www.google.co.uk/maps?hl=en&q=mcdonalds+derby
  for (const restaurantSlug of jeRestaurantSlugs) {
    console.log(`Restaurant "${restaurantSlug}" scrape starts.`);

    const jeRestaurantUrl = `https://www.just-eat.co.uk/restaurants-${restaurantSlug}/menu`;

    // Prepare to capture dynamic API call, then got to JE page to trigger it,
    // and continue when found.
    const [menuDynamicRes] = await Promise.all([
      resilientWaitForResponse(page, (response) => {
        const url = new URL(response.url());
        if (!url.pathname.includes('menu/dynamic')) return false;
        if (response.request().method() === 'OPTIONS') return false;
        return response;
      }),
      resilientGoto(page, jeRestaurantUrl),
    ]);

    // Grab ratings data from captured API call
    let ratingsAverage;
    let ratingsCount;
    await menuDynamicRes.json().then(async (menuDynamicJson) => {
      ratingsAverage = menuDynamicJson.RestaurantRating.RatingAverageStars;
      ratingsCount = menuDynamicJson.RestaurantRating.RatingCount;
    });

    // Just Eat exposes a lot of restaurant data in window.__NEXT_DATA__
    // so traverse and loop this to gather the data we want.
    // eslint-disable-next-line no-underscore-dangle
    const data = await page.evaluate(() => (
      JSON.parse(document.getElementById('__NEXT_DATA__').innerText)
    ));

    const schema = await page.evaluate(() => (
      JSON.parse(document.querySelector('script[type="application/ld+json"]').innerText)
    ));
    const { streetAddress, addressLocality, postalCode } = schema.address;

    // Likely a URL 404, restaurant no longer exists with that slug, return early
    if (!data || !schema) {
      console.log(`Data/schema not found for "${restaurantSlug}", skipping.`);
    } else {
      const { preloadedState } = data.props.appProps;
      const {
        items, modifierGroups, modifierSets,
      } = preloadedState.menu.restaurant.cdn;

      const restaurantData = { itemPrices: {} };
      restaurantData.name = schema.name
        .replaceAll("McDonald's® -", '').trim(); // Remove common name prefix
      restaurantData.address = `${streetAddress}, ${addressLocality}, ${postalCode}`;
      restaurantData.latitude = schema.geo.latitude;
      restaurantData.longitude = schema.geo.longitude;
      restaurantData.ratingsAverage = ratingsAverage;
      restaurantData.ratingsCount = ratingsCount;

      items.forEach((item) => {
        const itemName = item.name;

        // Ignore meal items for now
        if (
          !itemName.includes('Meal')
          && itemName !== 'Crispy Chicken Salad - calories exc. additional condiments'
        ) {
          item.variations.forEach((variation) => {
            // For items with no modifierGropIds, add base item to DB
            if (variation.modifierGroupsIds.length === 0) {
              addItemPriceToRestaurantData(
                restaurantData,
                itemName,
                variation.basePrice,
              );
            }

            variation.modifierGroupsIds.forEach((mGId) => {
              const modifierGroup = modifierGroups.find((mg) => mg.id === mGId);

              // Only process "Select Size" groups, some others are "Remove" and "Extra".
              if (modifierGroup.name === 'Select Size') {
                // Add all modified items to DB
                modifierGroup.modifiers.forEach((modifierId) => {
                  const modifierSet = modifierSets[modifierId];
                  const { additionPrice, name: modifierName, removePrice } = modifierSet.modifier;

                  addItemPriceToRestaurantData(
                    restaurantData,
                    `${itemName} ${modifierName}`,
                    (variation.basePrice + additionPrice + removePrice).toFixed(2),
                  );
                });

              // For items with no size modified items, add base item to DB
              } else {
                addItemPriceToRestaurantData(
                  restaurantData,
                  itemName,
                  variation.basePrice,
                );
              }
            });
          });
        }
      });

      // Add complete restaurant data to collection Array
      jeRestaurantsData[restaurantSlug] = restaurantData;
      console.log(`Restaurant "${restaurantSlug}" scrape finished.`);
    }
  }

  // Write all complete restaurant data to JSON file
  writeFileSync(
    'tmp/jeRestaurantsData.json',
    JSON.stringify(jeRestaurantsData, null, 2),
  );
  await page.close();
  console.log('scrapeJustEatRestaurants() finished.');
}

const nutritionData = {};

function addItemToNutritionData(item, categoryName) {
  console.log('addItemToNutritionData', item.item_marketing_name, categoryName);
  const niceName = normaliseName(item.item_marketing_name);

  // Add item only if it doesn't already exist in data
  if (!hasOwnProperty.call(nutritionData, niceName)) {
    const data = { category: categoryName };

    // Loop over item nutrient data to collect data
    for (const n of item.nutrient_facts.nutrient) {
      data[n.nutrient_name_id] = n.value;
    }

    // Convert Australian data to UK. kcal to kJ, sodium to salt
    if (!data.energy_kJ) {
      data.energy_kJ = parseFloat((data.energy_kcal * 4.184).toFixed(0));
      data.energy_kcal = parseFloat(data.energy_kcal.toFixed(0));
    }
    if (!data.salt) {
      // 1000mg sodium is 2.5g salt
      data.salt = parseFloat((data.sodium / (1000 / 2.5)).toFixed(3));
    }
    // If there's still no salt value, set to 0, some items have no salt value
    if (!data.salt) data.salt = 0;

    nutritionData[niceName] = data;
  }
}

async function scrapeMcDonaldsSingleProductsNutrition() {
  console.log('scrapeMcDonaldsSingleProductsNutrition() starts.');

  const page = await browser.newPage();
  await setUserAgentHeadfulChrome(page);
  abortRedundantRequests(page, '.css');

  const categoryA = 'Fries & Sides';
  const categoryB = 'Milkshakes & Cold Drinks';
  const products = [
    [categoryA, 'https://www.mcdonalds.com/ie/en-ie/product/carrot-bag.html'],
    [categoryB, 'https://www.mcdonalds.com/ie/en-ie/product/raspberry-ripple-iced-cooler-large.html'],
    [categoryB, 'https://www.mcdonalds.com/ie/en-ie/product/raspberry-ripple-iced-cooler-regular.html'],
  ];

  for (const product of products) {
    const category = product[0];
    const url = product[1];

    console.log(`Product Url "${url}" scrape starts.`);

    // Prepare to capture API call, then click category to trigger it, and continue when found
    await Promise.all([
      resilientWaitForResponse(page, async (httpResponse) => {
        const resUrl = await httpResponse.url();

        // Resolve Promise if product has been discontinued
        if (resUrl === url && httpResponse.status() !== 200) {
          console.log('Product discontinued.');
          return true;
        }

        // Look for product API data when product is active
        if (resUrl.includes('itemDetails') && httpResponse.request().method() !== 'OPTIONS') {
          await httpResponse.text().then((jsonBody) => {
            // Sometimes is not JSON even with 200, so catch this and skip
            if (!stringIsJson(jsonBody)) {
              console.log('API response was not JSON. Skipping item.');
              return true;
            }

            const { item } = JSON.parse(jsonBody);

            // Marketing name does not include size information for some
            // single products, so use non-marketing name which does.
            item.item_marketing_name = item.item_name;

            // Add nutritional data for single product
            addItemToNutritionData(item, category);
            return console.log(`Product Url "${url}" scrape finished.`);
          });
          return true;
        }
        // API call not yet found, return false to keep looking
        return false;
      }),
      resilientGoto(page, url),
    ]);
  }

  await page.close();
  console.log('scrapeMcDonaldsSingleProductsNutrition() finished.');
}

async function clickVisibleButtonWithValue(valueAttribute, page) {
  await setTimeout(600); // Give SPA time to do stuff
  const el = await page.waitForSelector(
    `button[value="${valueAttribute}"]`,
    { visible: true },
  );
  await el.click();
}

async function clickButtonWithValue(valueAttribute, page) {
  await setTimeout(600); // Give SPA time to do stuff
  const el = await page.waitForSelector(
    `button[value="${valueAttribute}"]`,
    { visible: true },
  );
  await page.evaluate((e) => e.click(), el);
}

async function scrapeMcDonaldsNutritionalCalculator() {
  console.log('scrapeMcDonaldsNutritionalCalculator() starts.');

  const page = await browser.newPage();
  await setUserAgentHeadfulChrome(page);
  abortRedundantRequests(page);

  // Go to McDonalds Nutrition Calculator home
  await resilientGoto(
    page,
    'https://www.mcdonalds.com/gb/en-gb/good-to-know/nutrition-calculator.html',
  );
  await page.waitForSelector('.cmp-product-card__button');

  // Get Hash of all category names and button valueAttribute
  let categories = await page.evaluate(() => {
    const excludeCategories = ['Happy Meal®', 'Vegan', 'Vegetarian'];
    const hashArray = [];
    const categoryButtons = document.querySelectorAll('.cmp-product-card__button');
    categoryButtons.forEach((buttonEl) => {
      const categoryName = buttonEl.querySelector('span').innerText;
      // Ignore categories that add no new items
      if (!excludeCategories.includes(categoryName)) {
        hashArray.push({
          name: buttonEl.querySelector('span.cmp-product-card__title').innerText,
          valueAttribute: buttonEl.getAttribute('value'),
        });
      }
    });
    return hashArray;
  });

  // Throw an Error if Category Names are not found, required to proceed
  if (categories.length === 0) {
    throw Error('CategoryNames should not be empty.');
  }

  // Reverse order assigns category a little cleaner
  categories = categories.reverse();

  // Loop over all category names
  for (const category of categories) {
    console.log(`Category "${category.name}" scrape starts.`);

    // Prepare to capture API call, then click category to trigger it,
    // and continue when found.
    const [catItemsRes] = await Promise.all([
      resilientWaitForResponse(page, (response) => {
        const url = new URL(response.url());
        if (!url.pathname.includes('itemList')) return false;
        // Category itemList always contains multiple - delimited items
        if (!url.searchParams.get('item').includes('-')) return false;
        return response;
      }),
      clickVisibleButtonWithValue(category.valueAttribute, page),
    ]);

    // Response body not yet available so Promise .then when it is
    await catItemsRes.text().then(async (catItemsJson) => {
      // Sometimes is not JSON even with 200, so catch this and skip
      if (!stringIsJson(catItemsJson)) {
        console.log(`API response was not JSON. Skipping ${category.name}.`);
        // Page in erroneous state, so reload to category-index and skip category
        await page.reload();
        return;
      }

      const categoryItems = JSON.parse(catItemsJson);
      const items = categoryItems.items && categoryItems.items.item;
      // If there are no items in category (e.g. it returned error) then skip it
      if (!items) {
        console.log(`Category items is not truthy. Skipping ${category.name}.`);
        await page.reload(); // Reload back to category-index
        return;
      }

      for (const item of items) {
        const itemId = item.id;
        const itemName = item.item_name;

        // Prepare to capture API call, then click item card to trigger it,
        // and continue when found.
        const [baseItemRes] = await Promise.all([
          resilientWaitForResponse(page, (r) => r.url().includes('itemDetails')),
          clickButtonWithValue(itemId, page),
        ]);

        // Response body not yet available so Promise .then when it is
        await baseItemRes.text().then(async (baseItemJson) => {
          // Sometimes is not JSON even with 200, so catch this and skip
          if (!stringIsJson(baseItemJson)) {
            console.log(`API response was not JSON. Skipping ${itemName}.`);
            // Page in erroneous state, so reload root, and click back in to category
            await page.reload();
            await clickVisibleButtonWithValue(category.valueAttribute, page);
            return;
          }

          const baseItem = JSON.parse(baseItemJson).item;
          // Add nutritional data for base item
          addItemToNutritionData(baseItem, category.name);

          // Get itemIds for each size if available
          const sizeIds = await page.$$eval('option', (oEls) => oEls.map((oEl) => oEl.value));
          // When there is more than 1 size, do extra steps to get nutrition data
          if (sizeIds.length > 1) {
            // Remove base item id as data already acquired
            const baseSizeId = sizeIds.indexOf(itemId.toString());
            sizeIds.splice(baseSizeId, 1);

            for (const sizeId of sizeIds) {
              // Prepare to capture API call, then select size to trigger it,
              // and continue when found.
              const [sizeItemRes] = await Promise.all([
                resilientWaitForResponse(page, (r) => r.url().includes('itemDetails')),
                page.select('select[name="size-selector"]', sizeId),
              ]);

              // Response body not yet available so Promise .then when it is
              await sizeItemRes.text().then(async (sizeItemJson) => {
                // Sometimes is not JSON even with 200, so catch this and skip
                if (!stringIsJson(sizeItemJson)) {
                  console.log(`API response was not JSON. Skipping sizeId ${sizeId}.`);
                } else {
                  const sizeItem = JSON.parse(sizeItemJson).item;
                  // Add nutritional data for size item
                  addItemToNutritionData(sizeItem, category.name);
                }
              });
            }
          }

          // Click "Delete all items"
          await page.click('.cmp-nc-item-editor__delete-all-btn');
          // Click "Yes, delete"
          await page.click('.ui-dialog-buttonset--primary');
          // Close modal to to go to category-index page
          await page.click('.ui-icon-closethick');
          // Click in to category page
          await clickVisibleButtonWithValue(category.valueAttribute, page);
        });
      }
    });

    console.log(`Category "${category.name}" scrape finished.`);

    await page.reload(); // Back to category-index after we're done with a category
  }

  await page.close();
  console.log('scrapeMcDonaldsNutritionalCalculator() finished.');
  return nutritionData; // Pass data to .then()
}

initialiseBrowser()
  .then(() => scrapeJustEatRestaurants())
  .then(() => scrapeMcDonaldsSingleProductsNutrition())
  .then(() => scrapeMcDonaldsNutritionalCalculator())
  .then(async (nutrition) => {
    // After both nutrition scrapes, write to file
    writeFileSync(
      'tmp/nutritionData.json',
      JSON.stringify(nutrition, null, 2),
    );

    await browser.close();
  });
