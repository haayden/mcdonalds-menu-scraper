const fs = require('fs');

test('buildClientData.js transforms data as expected', () => {
  // Setup test with pre-prepared scrape data
  // Equivalent of files created after successful: npm run server:scrape
  fs.copyFileSync('./test/jeRestaurantsData.json', './tmp/jeRestaurantsData.json');
  fs.copyFileSync('./test/nutritionData.json', './tmp/nutritionData.json');

  // Equivalent of running: npm run server:build-client-data
  require('./buildClientData');

  // Read in the just-created data and pre-prepared expected data
  /* eslint-disable import/no-unresolved */
  const createdClientJsData = require('../../tmp/clientJsData.json');
  const expectedClientJsData = require('../../test/clientJsData.json');
  const createdClientTemplateData = require('../../tmp/clientTemplateData.json');
  const expectedClientTemplateData = require('../../test/clientTemplateData.json');
  /* eslint-disable import/no-unresolved */

  // Expect just-created and expected to match. If so, all is working
  expect(createdClientJsData).toEqual(expectedClientJsData);
  expect(createdClientTemplateData).toEqual(expectedClientTemplateData);
});
